output "cname" {
  value = aws_elastic_beanstalk_environment.this.cname
}

output "endpoint_url" {
  value = aws_elastic_beanstalk_environment.this.endpoint_url
}
