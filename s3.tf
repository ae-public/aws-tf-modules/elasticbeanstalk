resource "aws_s3_bucket" "default" {
  bucket        = lower(join("-", [var.org_name, var.ou_name, var.acc_name, "elasticbeanz-bucket"]))
  force_destroy = var.force_bucket_destruction

  tags = merge(
    local.tags,
    var.tags,
    map(
      "Name", join("-", [var.org_name, "s3-bucket"]),
  ))
}
