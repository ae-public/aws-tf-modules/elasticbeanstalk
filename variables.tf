variable "org_name" {
  type = string
}

variable "ou_name" {
  description = "Name of OU"
  type        = string
}

variable "acc_name" {
  description = "Account Name"
  type        = string
}

variable "aws_region" {
  type    = string
  default = "us-east-2"
}

variable "ebe_name" {
  type    = string
  default = "my-env"
}

variable "app_name" {
  type    = string
  default = "my-app"
}

variable "org_account_id" {
  type = string
}

variable "org_admin_role" {
  type    = string
  default = "OrgAdminRole"
}

variable "instance_type" {
  type    = string
  default = "t2.micro"
}

variable "instance_type_family" {
  type    = string
  default = "t2"
}

variable "instance_types" {
  type    = string
  default = "t2.micro, t2.small, t3.nano"
}

variable "pub_subnets" {
  type = string
}

variable "prv_subnets" {
  type = string
}

variable "force_bucket_destruction" {
  type    = bool
  default = false
}

variable "aws_vpc_id" {
  type = string
}

variable "min_instances" {
  type    = string
  default = "2"
}

variable "max_instances" {
  type    = string
  default = "3"
}

variable "tags" {
  description = "Map of tags to add to all resources"
  type        = map(string)
  default     = {}
}

variable "ebe_settings" {
  type    = list
  default = []
}

variable "solution_stack_name" {
  type = string
}
