# iam roles
resource "aws_iam_role" "infra-ec2-role" {
  name = join("-", [var.org_name, "ec2-role"])
  tags = merge(
    local.tags,
    var.tags,
    map(
      "Name", join("-", [var.org_name, "ec2-role"]),
  ))

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_instance_profile" "infra-ec2-role" {
  name = join("-", [var.org_name, "ec2-role"])
  role = aws_iam_role.infra-ec2-role.name
}


# service
resource "aws_iam_role" "infra-ebs-service-role" {
  name = join("-", [var.org_name, "ebs-service-role"])
  tags = merge(
    local.tags,
    var.tags,
    map(
      "Name", join("-", [var.org_name, "ebs-service-role"]),
  ))

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "elasticbeanstalk.amazonaws.com"
      },
      "Action": "sts:AssumeRole",
      "Condition": {
        "StringEquals": {
          "sts:ExternalId": "elasticbeanstalk"
        }
      }
    }
  ]
}
EOF
}


# policies

# aws-elasticbean-ec2-role
resource "aws_iam_policy_attachment" "app-attach1" {
  name       = "app-attach1"
  roles      = ["${aws_iam_role.infra-ec2-role.name}"]
  policy_arn = "arn:aws:iam::aws:policy/AWSElasticBeanstalkWebTier"
}

resource "aws_iam_policy_attachment" "app-attach2" {
  name       = "app-attach2"
  roles      = ["${aws_iam_role.infra-ec2-role.name}"]
  policy_arn = "arn:aws:iam::aws:policy/AWSElasticBeanstalkMulticontainerDocker"
}

resource "aws_iam_policy_attachment" "app-attach3" {
  name       = "app-attach3"
  roles      = ["${aws_iam_role.infra-ec2-role.name}"]
  policy_arn = "arn:aws:iam::aws:policy/AWSElasticBeanstalkWorkerTier"
}

resource "aws_iam_policy_attachment" "app-attach4" {
  name       = "app-attach4"
  roles      = ["${aws_iam_role.infra-ec2-role.name}"]
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
}

resource "aws_iam_policy_attachment" "app-attach7" {
  name       = "app-attach7"
  roles      = ["${aws_iam_role.infra-ec2-role.name}"]
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}

resource "aws_iam_policy_attachment" "app-attach8" {
  name       = "app-attach8"
  roles      = ["${aws_iam_role.infra-ec2-role.name}"]
  policy_arn = "arn:aws:iam::aws:policy/AmazonSQSFullAccess"
}

# aws-elasticbean-service-role
resource "aws_iam_policy_attachment" "app-attach5" {
  name       = "app-attach5"
  roles      = ["${aws_iam_role.infra-ebs-service-role.name}"]
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSElasticBeanstalkEnhancedHealth"
}

resource "aws_iam_policy_attachment" "app-attach6" {
  name       = "app-attach6"
  roles      = ["${aws_iam_role.infra-ebs-service-role.name}"]
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSElasticBeanstalkService"
}
