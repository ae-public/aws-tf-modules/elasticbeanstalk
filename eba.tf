resource "aws_elastic_beanstalk_application" "this" {
  name        = var.app_name
  description = "EB Application"

  appversion_lifecycle {
    service_role          = aws_iam_role.infra-ebs-service-role.arn
    max_count             = 128
    delete_source_from_s3 = true
  }

  tags = merge(
    local.tags,
    var.tags,
    map(
      "Name", var.app_name,
  ))
}
