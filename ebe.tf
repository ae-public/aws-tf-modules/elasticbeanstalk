resource "aws_elastic_beanstalk_environment" "this" {
  name                = var.ebe_name
  application         = aws_elastic_beanstalk_application.this.name
  solution_stack_name = var.solution_stack_name

  tags = merge(
    local.tags,
    var.tags,
    map(
      "Name", var.ebe_name,
  ))

  dynamic "setting" {
    for_each = var.ebe_settings
    content {
      namespace = setting.value["namespace"]
      name      = setting.value["name"]
      value     = setting.value["value"]
    }
  }

  lifecycle {
    ignore_changes = all
  }
}
